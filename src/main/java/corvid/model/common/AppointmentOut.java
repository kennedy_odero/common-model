package corvid.model.common;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by kodero on 5/16/16.
 */
@Entity
@Table(name = "appointments_out")
public class AppointmentOut implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "patient_name")
    private String patientName;

    @NotNull
    @Column(name = "patient_no")
    private String patientNo;

    @NotNull
    @Column(name = "age")
    private Integer age;

    @NotNull
    @Column(name = "gender")
    private String gender;

    @NotNull
    @Column(name = "appointment_no")
    private Long appointmentNo;

    @NotNull
    @Column(name = "date_of_appointment")
    private Date dateOfAppointment;

    @Column(name = "department")
    private String department;

    @Column(name = "comments")
    private String comments;

    @NotNull
    @Column(name = "payer_of_bill")
    private String payerOfBill = "SELF";

    public AppointmentOut(){

    }

    public AppointmentOut(String patientNo, String patientName, Integer age, String gender, Long appointmentNo, Date dateOfAppointment, String department, String comments, String payerOfBill){
        setPatientNo(patientNo);
        setPatientName(patientName);
        setAge(age);
        setGender(gender);
        setAppointmentNo(appointmentNo);
        setDateOfAppointment(dateOfAppointment);
        setDepartment(department);
        setComments(comments);
        setPayerOfBill(payerOfBill);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientNo() {
        return patientNo;
    }

    public void setPatientNo(String patientNo) {
        this.patientNo = patientNo;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Long getAppointmentNo() {
        return appointmentNo;
    }

    public void setAppointmentNo(Long appointmentNo) {
        this.appointmentNo = appointmentNo;
    }

    public Date getDateOfAppointment() {
        return dateOfAppointment;
    }

    public void setDateOfAppointment(Date dateOfAppointment) {
        this.dateOfAppointment = dateOfAppointment;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getPayerOfBill() {
        return payerOfBill;
    }

    public void setPayerOfBill(String payerOfBill) {
        this.payerOfBill = payerOfBill;
    }
}
