package corvid.model.common;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by kodero on 5/14/16.
 */
@Entity
@Table(name = "lab_results_in")
public class LabItemResult  implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "request_id")
    private String requestId;

    @NotNull
    @Column(name = "sample_no")
    private Long sampleNo;

    @NotNull
    @Column(name = "result_no")
    private Long resultNo;

    @NotNull
    @Column(name = "entered_on")
    private Date enteredOn;

    @NotNull
    @Column(name = "entered_by")
    private String enteredBy;

    @NotNull
    @Column(name = "test_name")
    private String testName;

    @NotNull
    @Column(name = "result_name")
    private String analyte;

    @NotNull
    @Column(name = "result_value")
    private String result;

    @Column(name = "units")
    private String units;

    @Column(name = "upper_limit")
    private String upperLimit;

    @Column(name = "lower_limit")
    private String lowerLimit;

    @Column(name = "warn")
    private Boolean warn;

    @Column(name = "transfered")
    private Boolean transfered;

    //not being used
    @Column(name = "reference")
    private String reference;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "processed", columnDefinition = "tinyint default 0")
    private Boolean processed;

    @Column(name = "date_sample_collected")
    private Date dateSampleCollected;

    @Column(name = "date_sample_received")
    private Date dateSampleReceived;

    @Column(name = "date_results_dispatched")
    private Date dateResultsDispatched;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Date getDateSampleCollected() {
        return dateSampleCollected;
    }

    public void setDateSampleCollected(Date dateSampleCollected) {
        this.dateSampleCollected = dateSampleCollected;
    }

    public Date getDateSampleReceived() {
        return dateSampleReceived;
    }

    public void setDateSampleReceived(Date dateSampleReceived) {
        this.dateSampleReceived = dateSampleReceived;
    }

    public Date getDateResultsDispatched() {
        return dateResultsDispatched;
    }

    public void setDateResultsDispatched(Date dateResultsDispatched) {
        this.dateResultsDispatched = dateResultsDispatched;
    }

    public String getAnalyte() {
        return analyte;
    }

    public void setAnalyte(String analyte) {
        this.analyte = analyte;
    }

    public String getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(String lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public String getUpperLimit() {
        return upperLimit;
    }

    public void setUpperLimit(String upperLimit) {
        this.upperLimit = upperLimit;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Boolean isWarn() {
        return warn;
    }

    public void setWarn(Boolean warn) {
        this.warn = warn;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Long getResultNo() {
        return resultNo;
    }

    public void setResultNo(Long resultNo) {
        this.resultNo = resultNo;
    }

    public Long getSampleNo() {
        return sampleNo;
    }

    public void setSampleNo(Long sampleNo) {
        this.sampleNo = sampleNo;
    }

    public Boolean isProcessed() {
        return processed;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }

    public Boolean isTransfered() {
        return transfered;
    }

    public void setTransfered(Boolean transfered) {
        this.transfered = transfered;
    }

    public Date getEnteredOn() {
        return enteredOn;
    }

    public void setEnteredOn(Date enteredOn) {
        this.enteredOn = enteredOn;
    }

    public String getEnteredBy() {
        return enteredBy;
    }

    public void setEnteredBy(String enteredBy) {
        this.enteredBy = enteredBy;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }
}
