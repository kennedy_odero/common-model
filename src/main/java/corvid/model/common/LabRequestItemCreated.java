package corvid.model.common;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by kodero on 5/13/16.
 */
@Entity
@Table(name = "lab_requests_out")
public class LabRequestItemCreated implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "request_id")
    private String requestId;

    @NotNull
    @Column(name = "patient_no")
    private String patientNo;

    @NotNull
    @Column(name = "full_name")
    private String fullNames;

    @NotNull
    @Column(name = "gender")
    private String gender;

    @NotNull
    @Column(name = "age")
    private Integer age;

    @NotNull
    @Column(name = "request_date")
    private Date dateOfRequest;

    @NotNull
    @Column(name = "test_code")
    private String testCode;

    @NotNull
    @Column(name = "test_name")
    private String testName;

    @NotNull
    @Column(name = "cost")
    private BigDecimal cost;

    @NotNull
    @Column(name = "doctor_requesting")
    private String doctorRequesting;

    @Column(name = "payer_of_bill")
    private String payerOfBill;

    @Column(name = "specimen_type")
    private String sampleType;

    @Column(name = "impression")
    private String impression;

    public LabRequestItemCreated(){

    }

    public LabRequestItemCreated(String requestId, String patientNo, String fullNames, String gender, Integer age, Date dateOfRequest,
                                 String testCode, String testName, BigDecimal cost, String doctorRequesting, String payerOfBill, String sampleType, String impression){
        setRequestId(requestId);
        setPatientNo(patientNo);
        setFullNames(fullNames);
        setGender(gender);
        setAge(age);
        setDateOfRequest(dateOfRequest);
        setTestCode(testCode);
        setTestName(testName);
        setCost(cost);
        setDoctorRequesting(doctorRequesting);
        setPayerOfBill(payerOfBill);
        setSampleType(sampleType);
        setImpression(impression);
    }

    public String toString(){
        StringBuffer sb = new StringBuffer()
                .append("RequestId : " + getRequestId())
                .append("PatientNo : " + getPatientNo())
                .append("Full Names : " + getFullNames())
                .append("Gender : " + getGender())
                .append("Age : " + getAge())
                .append("Date of Request : " + getDateOfRequest())
                .append("Test Code : " + getTestCode())
                .append("Test Name : " + getTestName())
                .append("Cost : " + getCost())
                .append("Doctor Requesting : " + getRequestId())
                .append("Payer of Bill : " + getRequestId());
        return sb.toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getPatientNo() {
        return patientNo;
    }

    public void setPatientNo(String patientNo) {
        this.patientNo = patientNo;
    }

    public String getFullNames() {
        return fullNames;
    }

    public void setFullNames(String fullNames) {
        this.fullNames = fullNames;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getDateOfRequest() {
        return dateOfRequest;
    }

    public void setDateOfRequest(Date dateOfRequest) {
        this.dateOfRequest = dateOfRequest;
    }

    public String getTestCode() {
        return testCode;
    }

    public void setTestCode(String testCode) {
        this.testCode = testCode;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getDoctorRequesting() {
        return doctorRequesting;
    }

    public void setDoctorRequesting(String doctorRequesting) {
        this.doctorRequesting = doctorRequesting;
    }

    public String getPayerOfBill() {
        return payerOfBill;
    }

    public void setPayerOfBill(String payerOfBill) {
        this.payerOfBill = payerOfBill;
    }

    public String getSampleType() {
        return sampleType;
    }

    public void setSampleType(String sampleType) {
        this.sampleType = sampleType;
    }

    public String getImpression() {
        return impression;
    }

    public void setImpression(String impression) {
        this.impression = impression;
    }
}
