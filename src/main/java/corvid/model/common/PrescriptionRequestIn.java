package corvid.model.common;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by kodero on 5/16/16.
 */
@Entity
@Table(name = "prescription_requests_in")
public class PrescriptionRequestIn implements Serializable{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "patient_name")
    private String patientName;

    @NotNull
    @Column(name = "patient_no")
    private String patientNo;

    @NotNull
    @Column(name = "age")
    private Integer age;

    @NotNull
    @Column(name = "gender")
    private String gender;

    @NotNull
    @Column(name = "appointment_no")
    private Long appointmentNo;

    @NotNull
    @Column(name = "date_of_request")
    private Date dateOfRequest;

    @NotNull
    @Column(name = "drug_code")
    private String drugCode;

    @NotNull
    @Column(name = "drug_name")
    private String drugName;

    @NotNull
    @Column(name = "duration")
    private Integer duration;

    @NotNull
    @Column(name = "schedule")
    private String schedule;

    @NotNull
    @Column(name = "dosage")
    private Integer dosage;

    @NotNull
    @Column(name = "staff_no")
    private String staffNo;

    @Column(name = "staff_name")
    private String staffName;

    @Column(name = "processed", columnDefinition = "tinyint default 0")
    private boolean processed;

    public PrescriptionRequestIn(){

    }

    public PrescriptionRequestIn(String patientName, String patientNo, Integer age, String gender, Long appointmentNo, Date dateOfRequest, String drugCode, String drugName, Integer dosage, String schedule, String staffNo, String staffName){
        setPatientNo(patientNo);
        setPatientName(patientName);
        setAge(age);
        setGender(gender);
        setAppointmentNo(appointmentNo);
        setDateOfRequest(dateOfRequest);
        setDrugCode(drugCode);
        setDrugName(drugName);
        setSchedule(schedule);
        setStaffNo(staffNo);
        setStaffName(staffName);
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientNo() {
        return patientNo;
    }

    public void setPatientNo(String patientNo) {
        this.patientNo = patientNo;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Long getAppointmentNo() {
        return appointmentNo;
    }

    public void setAppointmentNo(Long appointmentNo) {
        this.appointmentNo = appointmentNo;
    }

    public Date getDateOfRequest() {
        return dateOfRequest;
    }

    public void setDateOfRequest(Date dateOfRequest) {
        this.dateOfRequest = dateOfRequest;
    }

    public String getDrugCode() {
        return drugCode;
    }

    public void setDrugCode(String drugCode) {
        this.drugCode = drugCode;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getStaffNo() {
        return staffNo;
    }

    public void setStaffNo(String staffNo) {
        this.staffNo = staffNo;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }
}
