package corvid.model.common;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by kodero on 6/1/16.
 */
@Entity
@Table(name = "requests_in")
public class ServiceRequestIn implements Serializable{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "patient_name")
    private String patientName;

    @NotNull
    @Column(name = "patient_no")
    private String patientNo;

    @NotNull
    @Column(name = "age")
    private Integer age;

    @NotNull
    @Column(name = "gender")
    private String gender;

    @NotNull
    @Column(name = "appointment_no")
    private Long appointmentNo;

    @NotNull
    @Column(name = "date_of_request")
    private Date dateOfRequest;

    @NotNull
    @Column(name = "product_code")
    private String productCode;

    @Column(name = "product_name")
    private String productName;

    @NotNull
    @Column(name = "staff_no")
    private String staffNo;

    @Column(name = "staff_name")
    private String staffName;

    @Column(name = "processed", columnDefinition = "tinyint default 0")
    private boolean processed;

    @NotNull
    @Column(name = "dept_code", columnDefinition = "varchar(10) default ''")
    private String deptCode;

    //@NotNull
    @Column(name = "test_sample")
    private String testSample;

    @Column(name = "impression")
    private String impression;

    public ServiceRequestIn(){

    }

    public ServiceRequestIn(String patientName, String patientNo, Integer age, String gender, Long appointmentNo, Date dateOfRequest, String testCode, String testName, String staffNo, String staffName){
        setPatientNo(patientNo);
        setPatientName(patientName);
        setAge(age);
        setGender(gender);
        setAppointmentNo(appointmentNo);
        setDateOfRequest(dateOfRequest);
        setProductCode(testCode);
        setProductName(testName);
        setStaffNo(staffNo);
        setStaffName(staffName);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientNo() {
        return patientNo;
    }

    public void setPatientNo(String patientNo) {
        this.patientNo = patientNo;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Long getAppointmentNo() {
        return appointmentNo;
    }

    public void setAppointmentNo(Long appointmentNo) {
        this.appointmentNo = appointmentNo;
    }

    public Date getDateOfRequest() {
        return dateOfRequest;
    }

    public void setDateOfRequest(Date dateOfRequest) {
        this.dateOfRequest = dateOfRequest;
    }

    public String getStaffNo() {
        return staffNo;
    }

    public void setStaffNo(String staffNo) {
        this.staffNo = staffNo;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getTestSample() {
        return testSample;
    }

    public void setTestSample(String testSample) {
        this.testSample = testSample;
    }

    public String getImpression() {
        return impression;
    }

    public void setImpression(String impression) {
        this.impression = impression;
    }
}
